var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = "select school_id, school_name, street, zip_code from school s" +
        " join address a on s.address_id = a.address_id;";
    console.log(query);
    connection.query(query, function(err, result){
        //insert into resume_
        callback(err, result);
    });
};

exports.delete = function(school_id, callback) {
    var query = "delete from school where school_id = " + school_id;

    connection.query(query, function(err, result){
        callback(err, result);
    });
};

exports.edit = function (schoolObj, callback) {
    var name = schoolObj.school_name;
    var school_id = schoolObj.school_id;
    var address_id = schoolObj.address_id;
    var query = "update school set school_name = '" + name + "' , address_id = " + address_id +
        " where school_id = " + school_id;
    console.log("School edit query:" + query);
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
