var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback){
    var query = 'select * from account;';

    connection.query(query, function(err, result){
        callback(err, result);
    });
};


exports.delete = function(account_id, callback){
    var query = 'delete from account where account_id = ' + account_id;

    connection.query(query, function(err, result){
        callback(err, result);
    });
};

exports.edit = function(accountObj, callback){
    var last = accountObj.last_name;
    var id = accountObj.account_id;
    var email = accountObj.email;
    var first = accountObj.first_name;
    var query = "update account set first_name = '" + first + "', last_name =  '" + last + "', email = '"
        + email + "' where account_id = " + id;
    connection.query(query, function(err, result){
        callback(err, result);
    });
    //let { account_id, email, first_name, last_name } = accountObj;
};