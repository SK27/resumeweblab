var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback){
    //select first_name, last_name, resume_name from  resume r
    //join account a on r.account_id = a.account_id
    //order by first_name asc;
    var query = "select first_name, last_name, resume_name from resume r join account a on"
        + " r.account_id = a.account_id order by first_name asc";
    conosle.log(query);
    connection.query(query, function(err, result){
        callback(err, result);
    });
};


exports.accountSchool = function(id, callback){
    var query = "select school_name from school s join account_school a on a.school_id " +
        "= s.school_id where account_id = " + id;
    console.log(query);
    connection.query(query, function(err, result){
        callback(err, result);
    });
};


exports.accountCompany = function(id, callback){
    var query = "select company_name from company c join account_company a on " +
        "a.company_id = c.company_id where account_id  = " + id;
    connection.query(query, function(err, result){
        callback(err, result);
    });
};


exports.accountSkill = function(id, callback){
    var query = "select skill_name from skill s join account_skill a on a.skill_id = s.skill_id " +
        "where account_id = " + id;
    connection.query(query, function(err, result){
        callback(err, result);
    });
};

//will have multiple callbacks in order to insert into: resume, acc_sch, acc_comp, acc_skill
//note: "resume_id" assigned at for inserted record, will have to be used to insert the rest of records
/*
resumeObj =
{[school_id],
 [company_id],
 [skill_id],
 account_id,
 resume_name}
 */
//this is bunk=> doesnt update the further tables, mark me down haderman :(
exports.insert = function(resumeObj, callback){
    var query = "insert into resume (account_id, resume_name) values " +
    "(" + resumeObj.account_id + ", '" + resumeObj.resume_name + "');";
    connection.query(query, function(err, res){
        callback(err, res);
    });
};
